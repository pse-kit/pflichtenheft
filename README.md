# Pflichtenheft

Pflichtenheft des PSE-Projekts "Webservice zur Definition und Durchsetzung des Mehr-Augen-Prinzips" im WS2018/19 von Julius Häcker, Moritz Leitner, Nicolas Schuler, Noah Wahl und Wendy Yi

### PDF
Für die PDF-Datei zweimal mit `pdflatex Pflichtenheft.tex ` kompilieren.